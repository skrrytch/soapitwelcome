## Why SoapIt?

**soap it** supports developers and testers in creating, managing and sending SOAP messages to software systems with a clean and efficient interface.

![soap it](/img/homepage-image.png)


## Goals

### Simple project management

Simple project management with SOAP services and scenarios for creating, processing and testing SOAP messages. SOAP request can be assigned to multiple scenarios. All project files are stored in one project directory in the file system: Each request is one XML file and an additional project file in JSON format. Easy to manage!

*Current achievement of this goal: 75%*

### Simple expressions for dates and numbers

Simple expressions to use variables in SOAP messages: Functions for calculating and outputting dates in various formats. Functions for numeric values based on global and local variables as well as random values.

*Current achievement of this goal: 50%*

### Optimized test workflow

Optimized workflow for both individual queries and extensive tests through the provision of a result protocol.

*Current achievement of this goal: 75%*

### Generation of SOAP messages

Automatic and configurable generation of SOAP messages based on WSDLs.

*Current achievement of this goal: 25%*

### Editor

Powerful text editor for XML messages.

*Current achievement of this goal: 75%*

## No goals...

* Comprehensive test management, especially for load tests.
* Support for REST or other webservice technologies
